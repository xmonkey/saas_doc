# 创建一场比赛报名活动

# 创建活动并配置活动基本信息
进入控制台后，点击右上角【创建活动】。

* [ ]todo:此处需要修改系统，去掉权限限制，允许自由创建活动草稿。并在创建后关联到报名管理员，以及设置权限为【编辑】

![image-20180702184613430](../media/image-20180702184613430.png)

进入【创建活动】页面，根据提示，填写活动的基本信息，标注为【选填】的，可以不填。配置项的说明可参见[【基本信息】](../func_module/basic_settings.md)配置说明。

其中特别注意，**链接地址一旦提交，将无法再次修改**。

![image-20180702185703286](../media/image-20180702185703286.png)

【详细介绍(选填)】项是你需要重点关注的部分，您可以将赛事章程中的重要信息、赛事宣传图片等内容放在此处。如果您是从公众号或官网复制的富文本内容，在粘贴前请先清除格式（可以粘贴到Windows记事本中再复制），否则可能会引发一些显示问题。

保存后，将创建【活动草稿】。
![image-20180702190439413](../media/image-20180702190439413.png)

# 配置比赛组别
创建活动草稿之后，您需要配置至少1个【组别】后方可提交审核，审核通过后，您仍旧可以自由更改活动配置。

点击左侧导航栏中的【组别管理】，再点击右侧的【新建】

![image-20180702190713370](../media/image-20180702190713370.png)

根据提示，填写组别信息，关于配置项的具体说明可参见[【组别管理】](../func_module/race_categories.md)配置说明。
![](media/15312064117886.jpg)

我们根据页面提示，依次填写必填的组别名称、名额、报名时间、价格，除此之外的其他配置项采用默认值，点击页面顶部或底部的【保存】按钮后，即可创建最简单的比赛组别。


# 配置附加购买项目
如果您需要添加旅游套餐、接送服务、参观券等额外的付费购买或免费的商品，您可以通过【附加项目】完成，具体说明可参见[附加项目管理](../func_module/addons.md)配置说明。


# 提交审核
当您配置好活动的【基本信息】以及【组别管理】后，可以点击屏幕右上方的【提交审核】按钮。确认后，即可提交活动草稿给工作人员审核。
![](media/15312079837980.jpg)


工作人员会根据与您签订的合同，在后台审核您提交的活动草稿。如果没有问题，将尽快审核通过。如果有问题，我们的工作人员将与您直接联系解决。

# 分发报名链接
通过审核之后，您就可以向选手发送报名链接了。

进入赛事管理平台，选择您要分发报名链接的活动：
![](media/15312084559884.jpg)

点击比赛名称进入活动后台【首页】，在页面右侧您可以看到【报名链接】一项。您可以直接复制链接，也可以保存二维码，供选手扫码报名。该链接同时支持电脑端、手机端访问。

如果在报名时段内，你需要临时关闭报名通道，只需要可以点击左侧的【开放报名】全局开关，将其设为否即可，无须修改报名开始和结束时间。

![](media/15312088165336.jpg)

# 选手报名

选手获得报名链接后，如果当前时间不在报名时段内，则选手不能报名，但可以正常浏览活动报名页面。
![](media/15312093392175.jpg)

如果当前时间在报名时段内，但您设置了【概览】-【开放报名】状态为【否】，则会提示选手报名已临时关闭。

![](media/15312094438081.jpg)

当前时间处在报名时段，并且【开放报名】全局开关设为【是】时，选手可以点击【报名按钮】。当某个组别的付款订单数量达到名额上限时，系统会自动将该组别修改为【名额已满】。

![](media/15312097739497.jpg)

# 管理报名订单
在左侧导航栏中点击【报名订单】-【所有订单】，可以查看所有已经生成的订单。关于订单的操作说明，可以查看[【订单管理】](../func_module/orders.md)说明。
![](media/15312102337255.jpg)


# 管理已报名选手的信息、分配参赛号码、设置领物单
在【所有订单】页面，您可以点击右上角的【导出】按钮，并选择导出全部订单的报名信息，或者只导出成交订单的报名信息。
![](media/15312106544960.jpg)

将选手报名信息导出为Excel表格之后，你可以根据实际需要，在Excel中给选手分配参赛号码，并通过【导入】-【选手号码】功能为选手分配参赛号码。在导入前，请先下载导入模版按照模版填写数据进行导入。
![](media/15312106804441.jpg)

在【组别管理】页面，你可以为每个组别设置领物单。关于设置领物单的详细说明，可见[设置领物单](../func_module/pickup_cofirmation.md)。
![](media/15312108888129.jpg)
![](media/15312109313195.jpg)

# 通知选手查询参赛号码并下载领物单
在导入选手参赛号码、设置好领物单之后，您就可以通知选手来查询和下载领物单了。

我们提供【邮件】和【短信】2种通知方式，您可以根据需要选择。如果您需要使用该项服务，请联系与您对接的工作人员。
![](media/15312111212116.jpg)

选手可以在活动报名页面查询信息，输入报名时填写的证件号码即可查询参赛号码。
![](media/15312113312283.jpg)
![](media/15312113924890.jpg)

选手的参赛号码（例图中【汉5012】）会直接显示在结果中，如果需要下单领物单，则需要选手点击查询结果，核实身份后进入订单详情下载。

# 赛后发布成绩、照片
您同样可以使用我们的【通知】功能向选手发送邮件、短信提醒。同时我们也提供成绩发布、照片发布功能，您可以咨询我们的工作人员。

# 财务结算
比赛结束之后，您就可以申请财务结算了（具体时间根据合同确定）。您可以向工作人员提出财务结算申请，我们的工作人员会为您完成后续操作。

关于财务计算的详细规则，可参见[【财务计算规则】](financial_rules.md)