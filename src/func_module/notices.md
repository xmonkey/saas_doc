# 邮件通知
系统可以根据组别和报名状态，发送邮件通知给对应报名用户。

#发送邮件通知
##组别
选择要发送的报名组别。
##状态
选择报名状态，根据通知的内容，来选择报名状态进行过滤。
##邮件标题
设置通知邮件的标题。
##链接地址
设置一个通知邮件内容的地址。
##邮件内容
支持html格式，可以在内容中插入图片和文字。

##发送通知邮件
点击“发送通知邮件”，将发送通知邮件给对应的报名用户

![](media/15326804579660.jpg)
