# 设置领物单
在组别管理页面，选择对应组别，点击“设置领物单”，
![-w1194](media/15326561564351.jpg)
点击“确认”按钮
![-w1169](media/15326561808646.jpg)

首先上传领物单模板，图片格式为jpg格式，大小1M以内，最长边不超过1755。
根据模板的内容，添加相应的项目，项目可选择报名信息中的表单项，固定的文本内容以及号码。样本内容可以自行编辑，会通过预览显示在预览图片上。对每个项目的文字大小、水平偏移、垂直偏移根据模板进行调整。
点击"更新预览"，领物单模板图片会更新显示。
![](media/15326600398778.jpg)
点击图片可以看到完整的图片预览情况。

调整完毕后，点击 "提交" 保存领物单设置。