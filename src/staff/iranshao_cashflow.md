# 爱燃烧现金流

报名服务费、退款服务费、系统服务费、其他服务费对于组委会是支出，对于光热运动是收入。

光热运动的支出包括：
* 微信、支付宝等支付渠道的手续费
* 发送短信的成本
* 组委会申请结算后，向组委会汇款的手续费
* 向超过退款期限的选手退款时，汇款的手续费

在现金流中，列出了一场比赛报名活动中，光热运动的收支统计：
![](media/15325129964758.jpg)

在页面下方可以查看收入、支出明细：
![](media/15325129806161.jpg)

![](media/15325129993387.jpg)

