# Summary

* [封面](README.md)
* [介绍](src/intro.md)
* [术语表](GLOSSARY.md)
* 使用说明
    * [从零开始](src/user_guide/from_zero.md)
    * [创建一场比赛报名活动](src/user_guide/create_activity.md)
    * [财务计算规则](src/user_guide/financial_rules.md)
    * [订单退款](src/user_guide/refund.md)
    * [常见客服问题](src/user_guide/faq.md)
* 功能模块详解
    * [首页](src/func_module/home.md)
    * 报名设置
        * [基本信息](src/func_module/basic_settings.md)
        * [组别管理](src/func_module/race_categories.md)
        * [设置领物单](src/func_module/pickup_cofirmation.md)
        * [附加项目管理](src/func_module/addons.md)
        * [邀请码/折扣码](src/func_module/coupons.md)
    * 报名订单
        * [xb所有订单](src/func_module/orders.md)[to do]
        * [xb批量退款](src/func_module/batch_refund.md)[to do]
    * [通知](src/func_module/notices.md)
    * [数据](src/func_module/data.md)
* 爱燃烧工作人员后台
    * [开发设置](src/staff/payment_settings.md)
    * [爱燃烧设置](src/staff/iranshao_settings.md)
    * [财务汇总](src/staff/settlements.md)
    * [退款记录](src/staff/refund_records.md)
    * [设置结算规则和费用](src/staff/settlement_configs.md)
    * [爱燃烧现金流](src/staff/iranshao_cashflow.md)[to do]
    * [账户](src/staff/accounts.md)
    * [操作记录](src/staff/operate_history.md)

